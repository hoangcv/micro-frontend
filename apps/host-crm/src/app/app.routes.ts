import { NxWelcomeComponent } from './nx-welcome.component';
import { Route } from '@angular/router';
import { loadRemoteModule } from '@nx/angular/mf';

export const appRoutes: Route[] = [
  {
    path: 'setting-crm',
    loadChildren: () =>
      loadRemoteModule('setting-crm', './Routes').then((m) => m.remoteRoutes),
  },
  {
    path: 'admin-crm',
    loadChildren: () =>
      loadRemoteModule('admin-crm', './Routes').then((m) => m.remoteRoutes),
  },
  {
    path: '',
    component: NxWelcomeComponent,
  },
];

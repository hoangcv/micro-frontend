import { ModuleFederationConfig } from '@nx/webpack';

const config: ModuleFederationConfig = {
  name: 'admin-crm',
  exposes: {
    './Routes': 'apps/admin-crm/src/app/remote-entry/entry.routes.ts',
  },
};

export default config;

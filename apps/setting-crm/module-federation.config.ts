import { ModuleFederationConfig } from '@nx/webpack';

const config: ModuleFederationConfig = {
  name: 'setting-crm',
  exposes: {
    './Routes': 'apps/setting-crm/src/app/remote-entry/entry.routes.ts',
  },
};

export default config;

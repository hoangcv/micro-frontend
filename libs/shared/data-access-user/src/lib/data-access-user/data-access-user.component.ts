import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'lib-data-access-user',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './data-access-user.component.html',
  styleUrl: './data-access-user.component.scss',
})
export class DataAccessUserComponent {}
